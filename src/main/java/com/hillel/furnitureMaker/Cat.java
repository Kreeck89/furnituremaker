package com.hillel.furnitureMaker;

import lombok.Data;

@Data
public class Cat {
    private String name;
    private String surname;
}
